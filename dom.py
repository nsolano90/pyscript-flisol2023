from js import document
from pyodide.ffi import create_proxy

count = 0

def button_click(event):
    global count
     
    count += 1
     
    document.getElementById("msg").innerHTML = 'Botón Clickeado ' + str(count)
     
def setup():
    # Limpia el mensaje de cargando
    document.getElementById("msg").innerHTML = ''
     
    # Crea un proxy Js
    click_proxy = create_proxy(button_click)
     
    # Coloca el listener en el botón
    e = document.getElementById("button")
    e.addEventListener("click", click_proxy)
     
setup()