# Demo de Pyscript para el FLISOl 2023
Pyscript es un sistema que permite ejecutar Python en HTML.

La presentación se encuentra en el siguiente enlace:

[Presentación de Google](https://docs.google.com/presentation/d/1OGfcdTdlLMwBjWk5hA3WL-Qrfaft_uF1rdufb954aaQ/edit?usp=sharing)

## Instalación
Primero se deberá descargar e instalar Python 3.11  en su máquina de acuerdo a su sistema operativo. El enlace de la descarga es el siguiente:

[Python 3.11](https://www.python.org/downloads/release/python-3110/)

Posteriormente se deberá instalar el paquete venv para Python 3.11 (Para guía refierase al enlace [venv](https://docs.python.org/3/library/venv.html)). Por ejemplo para distribuciones basadas en Debian:

`sudo apt install python3.11-venv`

Luego se deberán descargar todos ejecutables de Pyscript. En cualquier distibución Linux se puede acceder a los ejecutables de la siguiente manera:

`wget https://pyscript.net/alpha/pyscript.{css,js,py}`

Finalmente se deberá descargar y descomprimir la librería Pyodide más actualizada del enlace [descarga](https://github.com/pyodide/pyodide/releases). Por ejemplo la versión 0.23.1 se debe de de descargar del siguiente enlace:

`https://github.com/pyodide/pyodide/releases/download/0.23.1/pyodide-0.23.1.tar.bz2`

`tar -xvjf pyodide-0.23.1.tar.bz2`


## Uso*
Se deberá de crear el ambiente virtual usando venv `python -m venv /path/to/new/virtual/environment`. Por ejemplo dentro de la carpeta en donde se descargaron los componentes anteriores:

`python -m venv myvenv`

Posteriormente se debe de levantar el ambiente virtual usando venv `source /path/to/directory`. Por ejemplo:

`source myvenv/bin/activate`

Finalmente se debe de levantar el servidor web de la siguiente manera:

`python3 -m http.server`

*Se recomienda usar una distribución Linux para la ejecución de Pyscript.



