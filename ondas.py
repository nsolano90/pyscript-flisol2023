import numpy as np

def onda(frecuencia, amplitud=1, fase=0):
    def _onda(tiempo):
        return amplitud * np.cos(2 * np.pi * frecuencia * tiempo + fase)

    return _onda